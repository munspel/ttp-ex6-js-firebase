/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/js/main.js","vendors~main"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/bootstrap.js":
/*!*****************************!*\
  !*** ./src/js/bootstrap.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase */ \"./node_modules/firebase/dist/index.cjs.js\");\n/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _babel_polyfill__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/polyfill */ \"./node_modules/@babel/polyfill/lib/index.js\");\n/* harmony import */ var _babel_polyfill__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_polyfill__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\ntry {\n  var firebaseConfig = {\n    apiKey: 'AIzaSyCpAeWu1nYiXLvkr14D-fc5O9hEv2L3Xsw',\n    authDomain: 'ttp7-e96a1.firebaseapp.com',\n    databaseURL: 'https://ttp7-e96a1.firebaseio.com',\n    projectId: 'ttp7-e96a1',\n    storageBucket: 'ttp7-e96a1.appspot.com',\n    messagingSenderId: '287053084671',\n    appId: '1:287053084671:web:6e275a0ef68a03f6e159ca',\n    measurementId: 'G-SGHLLM10BS'\n  }; // Initialize Firebase\n\n  firebase__WEBPACK_IMPORTED_MODULE_0__[\"initializeApp\"](firebaseConfig);\n  var app = firebase__WEBPACK_IMPORTED_MODULE_0__[\"app\"]();\n  var features = ['auth', 'database', 'messaging', 'storage'].filter(function (feature) {\n    return typeof app[feature] === 'function';\n  });\n  console.log(\"Firebase SDK loaded with \".concat(features.join(', ')));\n} catch (e) {\n  console.error(e);\n}\n\n//# sourceURL=webpack:///./src/js/bootstrap.js?");

/***/ }),

/***/ "./src/js/controllers/todo.controller.js":
/*!***********************************************!*\
  !*** ./src/js/controllers/todo.controller.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ \"jquery\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _repositories_todo_repository__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../repositories/todo.repository */ \"./src/js/repositories/todo.repository.js\");\n/* harmony import */ var _views_todo_todo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../views/todo/todo */ \"./src/js/views/todo/todo.js\");\n/* harmony import */ var _models_todo__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/todo */ \"./src/js/models/todo.js\");\nfunction asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }\n\nfunction _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, \"next\", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, \"throw\", err); } _next(undefined); }); }; }\n\n\n\n\n\n\nfunction renderTodo() {\n  return _renderTodo.apply(this, arguments);\n}\n\nfunction _renderTodo() {\n  _renderTodo = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {\n    var todos;\n    return regeneratorRuntime.wrap(function _callee$(_context) {\n      while (1) {\n        switch (_context.prev = _context.next) {\n          case 0:\n            _context.next = 2;\n            return _repositories_todo_repository__WEBPACK_IMPORTED_MODULE_1__[\"default\"].get();\n\n          case 2:\n            todos = _context.sent;\n            jquery__WEBPACK_IMPORTED_MODULE_0___default()('.todo').html(Object(_views_todo_todo__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(todos));\n\n          case 4:\n          case \"end\":\n            return _context.stop();\n        }\n      }\n    }, _callee);\n  }));\n  return _renderTodo.apply(this, arguments);\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  render: renderTodo,\n  add: function add(form) {\n    var _this = this;\n\n    console.log(jquery__WEBPACK_IMPORTED_MODULE_0___default()(form).serializeArray());\n    var data = jquery__WEBPACK_IMPORTED_MODULE_0___default()(form).serializeArray();\n    var model = new _models_todo__WEBPACK_IMPORTED_MODULE_3__[\"Todo\"]('', data[0].value, data[1].value);\n    _repositories_todo_repository__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add(model).then(function (data) {\n      console.log(data.id);\n\n      _this.render();\n    });\n    return false;\n  }\n});\n\n//# sourceURL=webpack:///./src/js/controllers/todo.controller.js?");

/***/ }),

/***/ "./src/js/main.js":
/*!************************!*\
  !*** ./src/js/main.js ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _bootstrap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bootstrap */ \"./src/js/bootstrap.js\");\n/* harmony import */ var _controllers_todo_controller__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./controllers/todo.controller */ \"./src/js/controllers/todo.controller.js\");\n\n\nwindow.Todo = _controllers_todo_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"];\n_controllers_todo_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].render(); // if (location.pathname==='/admin.html'){\n//   // Todo\n// } else {\n//   Todo.render();\n// }\n\n//# sourceURL=webpack:///./src/js/main.js?");

/***/ }),

/***/ "./src/js/models/todo.js":
/*!*******************************!*\
  !*** ./src/js/models/todo.js ***!
  \*******************************/
/*! exports provided: Todo, TodoConverter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Todo\", function() { return Todo; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"TodoConverter\", function() { return TodoConverter; });\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar Todo = function Todo(id, name, description) {\n  _classCallCheck(this, Todo);\n\n  this.id = id;\n  this.name = name;\n  this.description = description;\n};\nvar TodoConverter = /*#__PURE__*/function () {\n  function TodoConverter() {\n    _classCallCheck(this, TodoConverter);\n  }\n\n  _createClass(TodoConverter, [{\n    key: \"toFirestore\",\n    value: function toFirestore(todo) {\n      return {\n        name: todo.name,\n        description: todo.description\n      };\n    }\n  }, {\n    key: \"fromFirestore\",\n    value: function fromFirestore(snapshot, options) {\n      var data = snapshot.data(options);\n      return new Todo(snapshot.id, data.name, data.description);\n    }\n  }]);\n\n  return TodoConverter;\n}();\n\n//# sourceURL=webpack:///./src/js/models/todo.js?");

/***/ }),

/***/ "./src/js/repositories/base.repository.js":
/*!************************************************!*\
  !*** ./src/js/repositories/base.repository.js ***!
  \************************************************/
/*! exports provided: BaseRepository */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"BaseRepository\", function() { return BaseRepository; });\n/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase */ \"./node_modules/firebase/dist/index.cjs.js\");\n/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_0__);\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\n/* eslint-disable no-debugger */\n\nvar BaseRepository = /*#__PURE__*/function () {\n  function BaseRepository(entityName, converter) {\n    _classCallCheck(this, BaseRepository);\n\n    this.convertor = converter;\n    this.entityName = entityName;\n    this.db = firebase__WEBPACK_IMPORTED_MODULE_0___default.a.firestore();\n    this.entityRef = this.db.collection(this.entityName);\n  }\n\n  _createClass(BaseRepository, [{\n    key: \"add\",\n    value: function add(data) {\n      return this.entityRef.add(this.convertor.toFirestore(data));\n    }\n  }, {\n    key: \"get\",\n    value: function get() {\n      var _this = this;\n\n      return new Promise(function (resolve, reject) {\n        _this.entityRef.get().then(function (response) {\n          var resp = [];\n          response.forEach(function (item) {\n            resp.push(_this.convertor.fromFirestore(item));\n          });\n          resolve(resp);\n        })[\"catch\"](reject);\n      });\n    }\n  }]);\n\n  return BaseRepository;\n}();\n\n//# sourceURL=webpack:///./src/js/repositories/base.repository.js?");

/***/ }),

/***/ "./src/js/repositories/todo.repository.js":
/*!************************************************!*\
  !*** ./src/js/repositories/todo.repository.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _base_repository__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base.repository */ \"./src/js/repositories/base.repository.js\");\n/* harmony import */ var _models_todo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/todo */ \"./src/js/models/todo.js\");\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (new _base_repository__WEBPACK_IMPORTED_MODULE_0__[\"BaseRepository\"]('todo', new _models_todo__WEBPACK_IMPORTED_MODULE_1__[\"TodoConverter\"]()));\n\n//# sourceURL=webpack:///./src/js/repositories/todo.repository.js?");

/***/ }),

/***/ "./src/js/views/todo/todo.item.js":
/*!****************************************!*\
  !*** ./src/js/views/todo/todo.item.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction render(items) {\n  console.log(items);\n\n  if (items.length === 0) {\n    return \"\\n    <li class=\\\"list-group-item d-flex justify-content-between lh-condensed\\\">\\n        <div>\\n          No items\\n        </div>\\n    </li>\";\n  }\n\n  var html = '';\n  items.forEach(function (item) {\n    html += \"\\n     <li class=\\\"list-group-item d-flex justify-content-between lh-condensed\\\">\\n        <div>\\n          <h6 class=\\\"my-0\\\">\".concat(item.name, \"</h6><small class=\\\"text-muted\\\">\").concat(item.description, \"</small>\\n        </div>\\n     </li>\");\n  });\n  return html;\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (render);\n\n//# sourceURL=webpack:///./src/js/views/todo/todo.item.js?");

/***/ }),

/***/ "./src/js/views/todo/todo.js":
/*!***********************************!*\
  !*** ./src/js/views/todo/todo.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _todo_item__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./todo.item */ \"./src/js/views/todo/todo.item.js\");\n\n\nfunction render(data) {\n  return \"\\n     <h4 class=\\\"d-flex justify-content-between align-items-center mb-3\\\">\\n     <span class=\\\"text-muted\\\">Your todo</span>\\n     <span class=\\\"badge badge-secondary badge-pill\\\">\".concat(data.length, \"</span></h4>\\n     <ul class=\\\"list-group mb-3\\\">\\n        \").concat(Object(_todo_item__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(data), \"\\n     </ul>\");\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (render);\n\n//# sourceURL=webpack:///./src/js/views/todo/todo.js?");

/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = jQuery;\n\n//# sourceURL=webpack:///external_%22jQuery%22?");

/***/ })

/******/ });