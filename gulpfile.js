var gulp = require('gulp'),
  plumber = require('gulp-plumber'),
  rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
// var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin'),
  cache = require('gulp-cache');
var minifycss = require('gulp-minify-css');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var pug = require('gulp-pug');
var del = require('del');
const webpack = require('webpack-stream');

gulp.task('clean', function () {
  return del(['dist/**']);
});

gulp.task('browser-sync', function () {
  browserSync.init({
    server: {
      baseDir: './dist'
    }
  });
  browserSync.watch('dist', browserSync.reload);
});
//
gulp.task('bs-reload', function () {
  reload({stream: true});
});

gulp.task('images', function () {
  return gulp.src('src/images/**/*')
    .pipe(cache(imagemin({optimizationLevel: 3, progressive: true, interlaced: true})))
    .pipe(gulp.dest('dist/images/'));
});
gulp.task('pug', function () {
  return gulp.src('./src/*.pug')
    .pipe(plumber({
      handleError: function (err) {
        console.log(err);
        this.emit('end');
      }
    }))
    .pipe(pug({
      pretty: true,
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('sass', function () {
  return gulp.src(['src/css/**/*.scss'])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }))
    .pipe(sass())
    .pipe(autoprefixer('last 2 versions'))
    .pipe(gulp.dest('dist/css/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('dist/css/'));
});

gulp.task('js', function () {
  return gulp.src('src/js/main.js')
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }))
    .pipe(webpack({
      config: require('./webpack.config.js')
    }))
    .pipe(gulp.dest('dist/js/'));
});

gulp.task('watching', function () {
  gulp.watch('src/css/**/*.scss', gulp.series('sass'));
  gulp.watch('src/*.pug', gulp.series('pug'));
  gulp.watch('src/js/**/*.js', gulp.series('js'));
});
gulp.task('build', gulp.series('clean', 'images', 'pug', 'sass', 'js'));
gulp.task('watch', gulp.series('build', gulp.parallel('browser-sync', 'watching')));

