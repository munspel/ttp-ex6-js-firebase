let currentUser = null;
function getUser(){
  return currentUser;
}
function setUser(user){
  currentUser = user;
}

export { getUser,  setUser };
