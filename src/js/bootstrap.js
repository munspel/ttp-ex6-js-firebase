import * as firebase from 'firebase';
import '@babel/polyfill';
try {
  var firebaseConfig = {
    apiKey: 'AIzaSyCpAeWu1nYiXLvkr14D-fc5O9hEv2L3Xsw',
    authDomain: 'ttp7-e96a1.firebaseapp.com',
    databaseURL: 'https://ttp7-e96a1.firebaseio.com',
    projectId: 'ttp7-e96a1',
    storageBucket: 'ttp7-e96a1.appspot.com',
    messagingSenderId: '287053084671',
    appId: '1:287053084671:web:6e275a0ef68a03f6e159ca',
    measurementId: 'G-SGHLLM10BS'
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  let app = firebase.app();
  let features = ['auth', 'database', 'messaging', 'storage'].filter(feature => typeof app[feature] === 'function');
  console.log(`Firebase SDK loaded with ${features.join(', ')}`);
} catch (e) {
  console.error(e);
}
