import View from './../views/admin/index';
import $ from 'jquery';
import {getUser} from '../auth';
class AuthController{
  render(){
    $('#app').html(View({user: getUser()}));
  }
}
export default new AuthController();
