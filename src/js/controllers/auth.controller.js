import LodinView from './../views/auth/login';
import $ from 'jquery';
import firebase from 'firebase';
import { setUser } from '../auth';
class AuthController{
  render(){
    $('#app').html(LodinView());
  }
  login(){
    this.render(LodinView(this));
  }
  handleLogin(){
    firebase.auth().signInWithEmailAndPassword('munspel@ukr.net', '1qazxsw2')
      .then(user=>{
        setUser(user);
        $('body').trigger('render');
      })
      .catch(function(error) {
        console.log(error);
      });
    return false;
  }
  logout(){
    firebase.auth().signOut()
      .then(()=>{
        setUser(null);
        $('body').trigger('render');
      })
      .catch(function(error) {
        console.log(error);
      });
    return false;
  }
}
export default new AuthController();
