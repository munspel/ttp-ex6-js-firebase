import $ from 'jquery';
import TodoRepository from '../repositories/todo.repository';
import TodoView from '../views/todo/todo';
import Layout from '../views/todo/index';
import { Todo } from '../models/todo';

async function renderTodo(){
  let todos =  await TodoRepository.get();
  $('.todo').html(TodoView(todos));
}

export default {
  render: function(){
    $('#app').html(Layout());
    renderTodo();
  },
  add: function(form){
    console.log($(form).serializeArray());
    let data = $(form).serializeArray();
    let model = new Todo('',data[0].value,  data[1].value);
    TodoRepository.add(model).then((data)=>{
      console.log(data.id);
      this.render();
    });
    return false;
  }
};
