import './bootstrap';
import Todo from './controllers/todo.controller';
import Auth from './controllers/auth.controller';
import Admin from './controllers/admin.controller';
import $ from 'jquery';
import {getUser, setUser} from './auth';
import firebase from 'firebase';

firebase.auth().onAuthStateChanged(function(user) {
  setUser(user);
  console.log(user);
  $('body').trigger('render');
});
let ctx = {};
ctx.Auth = Auth;
ctx.Todo = Todo;
ctx.Admin = Admin;
ctx.setRoute = function(route){
  $('body').trigger('route', [route]);
};

$('body').on('route', (event, data)=>{
  console.log(data);
  location.hash = '#'+data;
  $('body').trigger('render');
});

$('body').on('render', ()=>{
  if (location.hash === '#admin'){
    console.log(getUser());
    if (getUser()){
      Admin.render();
    } else {
      Auth.render();
    }
  } else if (location.hash === '#about'){
    $('#app').html('About page');
  } else if (location.hash === '#home'){
    Todo.render();
  } else {
    Todo.render();
  }
});
export { ctx };

