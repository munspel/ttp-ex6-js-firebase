export class Todo {
  constructor(id, name, description) {
    this.id = id;
    this.name = name;
    this.description = description;
  }
}

export class TodoConverter {
  toFirestore(todo) {
    return {
      name: todo.name,
      description: todo.description,
    };
  }

  fromFirestore(snapshot, options) {
    const data = snapshot.data(options);
    return new Todo(snapshot.id, data.name, data.description);
  }
}
