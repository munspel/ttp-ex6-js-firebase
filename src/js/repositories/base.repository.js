/* eslint-disable no-debugger */

import firebase from 'firebase';

export class BaseRepository {

  constructor(entityName, converter){
    this.convertor = converter;
    this.entityName = entityName;
    this.db = firebase.firestore();
    this.entityRef = this.db.collection(this.entityName);
  }

  add(data){
    return this.entityRef.add(this.convertor.toFirestore(data));
  }

  get(){
    return new Promise((resolve, reject)=>{
      this.entityRef.get().then(response=>{
        let resp = [];
        response.forEach((item)=>{
          resp.push(this.convertor.fromFirestore(item));
        });
        resolve(resp);
      }).catch(reject);
    });
  }

}
