import {BaseRepository} from './base.repository';
import {TodoConverter} from '../models/todo';

export default new BaseRepository('todo', new TodoConverter());
