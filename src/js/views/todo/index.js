export default function render() {
  return `
    <div class="col-md-4 order-md-2 mb-4 todo"></div>
    <div class="col-md-8 order-md-1">
        <h4 class="mb-3">Todo form</h4>
        <h4 class="mb-3" onclick="App.ctx.setRoute('admin')"> admin</h4>
        
        <form class="todo-form needs-validation" novalidate="" onsubmit="App.ctx.Todo.add(this); return false;">
            <div class="row">
            <div class="col-md-6 mb-3">
            <label for="name">Name</label>
            <input class="form-control" id="name" type="text" placeholder="" value="" required="" name="name">
            <div class="invalid-feedback">Valid name is required.</div>
        </div>
        <div class="col-md-6 mb-3">
            <label for="description">Description</label>
            <input class="form-control" id="description" type="text" placeholder="" value="" required="" name="description">
            <div class="invalid-feedback">Valid description name is required.</div>
        </div>
        </div>
        <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Add</button>
        </form>
    </div>`;
}
