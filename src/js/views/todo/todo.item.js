function render(items) {
  console.log(items);
  if (items.length === 0) {
    return `
    <li class="list-group-item d-flex justify-content-between lh-condensed">
        <div>
          No items
        </div>
    </li>`;
  }
  let html = '';
  items.forEach(item => {
    html += `
     <li class="list-group-item d-flex justify-content-between lh-condensed">
        <div>
          <h6 class="my-0">${item.name}</h6><small class="text-muted">${item.description}</small>
        </div>
     </li>`;
  });
  return html;
}

export default render;
