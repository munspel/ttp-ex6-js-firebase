import itemsRender from './todo.item';
function render(data) {
  return `
     <h4 class="d-flex justify-content-between align-items-center mb-3">
     <span class="text-muted">Your todo</span>
     <span class="badge badge-secondary badge-pill">${data.length}</span></h4>
     <ul class="list-group mb-3">
        ${itemsRender(data)}
     </ul>`;
}
export default render;
