const path = require('path');

module.exports = {
  mode: 'development',
  externals: {
    jquery: 'jQuery'
  },
  output: {
    filename: '[name].js',
    chunkFilename: '[name].bundle.js',
    library: 'App',

  },
  performance: {
    hints: false,
    maxEntrypointSize: 1024000,
    maxAssetSize: 1024000
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },
  module: {
    rules: [{
      test: /\.m?js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }
    },
    {
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'eslint-loader',
      options: {
        // eslint options (if necessary)
      },
    },
    ]
  }
};
